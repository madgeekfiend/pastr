class PastiesController < ApplicationController

  def create
    pastie = Pastie.new :content=>params[:pastie][:content].to_s.strip, :title=>params[:pastie][:title]
    pastie.save!
    redirect_to show_by_slug_path( :slug=>pastie.slug )
  end

  def new

  end

  def show

  end

  def by_slug
    #We are going to assume it's always going to be
    @pastie = Rails.cache.fetch( params[:slug] ) do
        Pastie.find_by_slug(params[:slug])
      end
  end



end