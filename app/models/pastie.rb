class Pastie < ActiveRecord::Base
  before_validation 'get_id(8)'




  private

  def get_id length
    return unless self.slug.nil?
    character_set = [(0..9).collect(&:to_s).to_a, ('a'..'z').to_a, ('A'..'Z').to_a].flatten
    tmp_id = (1..length).collect{ character_set[rand(character_set.length)] }.join
    while Pastie.exists?(:slug=>tmp_id) do
      tmp_id.next!
    end
    self.slug = tmp_id
  end

end
