class CreatePasties < ActiveRecord::Migration
  def up
    create_table :pasties do |t|
      t.text :content
      t.datetime :created_at
      t.datetime :updated_at
      t.string :slug #Slug 8 letter descriptor
      t.string :title
      t.string :author
    end
    add_index :pasties, :slug

    Pastie.create :content=>"The first Pasty Ever!",
                  :title=>"First!",
                  :author=>"Sam C"
  end

  def down
    drop_table :pasties
  end
end
